import uuid


class TestUserListController:
    def test_get_empty_list_of_users(self, flask_client):
        get_list_response = flask_client.get('/users')
        assert get_list_response.status_code == 200
        assert get_list_response.json == []

    def test_create_a_user(self, flask_client):
        test_email = f'testuser{uuid.uuid4()}@example.com'
        create_user_response = flask_client.post('/users', json={'email': test_email})
        assert create_user_response.status_code == 201
        user_id = create_user_response.json['id']
        user_details_response = flask_client.get(f'/users/{user_id}')
        assert user_details_response.status_code == 200
        assert user_details_response.json['email'] == test_email
